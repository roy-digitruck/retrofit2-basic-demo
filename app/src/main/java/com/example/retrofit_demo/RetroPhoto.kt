package com.example.retrofit_demo

data class RetroPhoto(
    var albumId: Int?,
    var id: Int?,
    var title: String?,
    var url: String?,
    var thumbnailUrl: String?
)