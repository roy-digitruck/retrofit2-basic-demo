package com.example.retrofit_demo

import retrofit2.Call
import retrofit2.http.GET

interface GetDataService {

    @get:GET("/orders/")
    val allPhotos: Call<List<RetroPhoto>>
}
