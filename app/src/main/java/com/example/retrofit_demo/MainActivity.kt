package com.example.retrofit_demo

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val service = RetrofitClient.instance.create(GetDataService::class.java)
        val call = service.allPhotos

        call.enqueue(object : Callback<List<RetroPhoto>> {
            override fun onFailure(call: Call<List<RetroPhoto>>?, t: Throwable?) {
                println("Fail!")
            }

            override fun onResponse(call: Call<List<RetroPhoto>>?, response: Response<List<RetroPhoto>>?) {
                val retroPhotoList = response?.body()
                for (i in 0..4) {//~ Print the first four objects...
                    val retroPhoto = retroPhotoList?.get(i)
                    println(retroPhoto)
                }
            }
        })
    }
}
